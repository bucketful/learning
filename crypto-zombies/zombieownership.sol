pragma solidity ^0.4.19;

import "./zombieattack.sol";
import "./erc721.sol";
import "./safemath.sol";

/// TODO: Replace this with natspec descriptions
/// @title A contract for ownership, querying balance and transferring zombies
/// @author GKwak
contract ZombieOwnership is ZombieAttack, ERC721 {
  /// @dev 
  using SafeMath for uint256;

  mapping (uint => address) zombieApprovals;

  function balanceOf(address _owner) public view returns (uint256 _balance) {
    return ownerZombieCount[_owner];
  }

  function ownerOf(uint256 _tokenId) public view returns (address _owner) {
    return zombieToOwner[_tokenId];
  }

  function _transfer(address _from, address _to, uint256 _tokenId) private {
    ownerZombieCount[_to] = ownerZombieCount[_to].add(1);
    ownerZombieCount[msg.sender] = ownerZombieCount[msg.sender].sub(1);
    zombieToOwner[_tokenId] = _to;
    Transfer(_from, _to, _tokenId);
  }

  function transfer(address _to, uint256 _tokenId) public onlyOwnerOf(_tokenId) {
    _transfer(msg.sender, _to, _tokenId);
  }

  function approve(address _to, uint256 _tokenId) public onlyOwnerOf(_tokenId) {
    zombieApprovals[_tokenId] = _to;
    Approval(msg.sender, _to, _tokenId);
  }

  function takeOwnership(uint256 _tokenId) public {
    require(zombieApprovals[_tokenId] == msg.sender);
    address owner = ownerOf(_tokenId);
    _transfer(owner, msg.sender, _tokenId);
  }
}

// Yeah... Good luck writing all your function calls this way!
// Scroll right ==>
{
    "jsonrpc":"2.0",
    "method":"eth_sendTransaction",
    "params":[
                {
                   "from":"0xb60e8dd61c5d32be8058bb8eb970870f07233155",
                   "to":"0xd46e8dd67c5d32be8058bb8eb970870f07244567",
                   "gas":"0x76c0",
                   "gasPrice":"0x9184e72a000",
                   "value":"0x9184e72a",
                   "data":"0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"
                }
             ],
    "id":1
]}

CryptoZombies
    .methods
    .createRandomZombie("Vitalik Nakamoto 🤔")
    .send({ 
            from: "0xb60e8dd61c5d32be8058bb8eb970870f07233155", 
            gas: "3000000" 
         })











