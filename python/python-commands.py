PYTHON COMMANDS:

STRING/CHAR:
	'string'.index(char/str) use .find() instead
	'string'.isalnum()
	'string'.isalpha()
	'string'.lower()
	'string'.lstrip()/.rstrip()/.strip()
	'string'[len(string):] = ''
	'string'[:len(string)] = 'string'
	char.isdigit()
	[a-string] += ['a']	# appending an item into a list

LIST:
	pop() # from right
	pop(0) # from left
	append(), NOT push()					<----- CAUTION

	queue = [(root, 0)]
	for node, i in queue:					<----- can get pair elements together
		...

	queue += (node.left, i-1), (node.right, i+1)	<-- multiple inputs

	use "if not Array" to indicate empty Array		<-- checking for empty Array
		NOT "if Array is None"



collections.Counter:
	elements()
	most_common(n)
	counter.items() # (key,value) tuples
	counter.values() # values
	set(counter)
	list(counter)
	list(counter.elements())
	counter += Counter() # removes 0 and negative counts
	counter.clear()


	# intersection: 
	counter3 = counter1 & counter2
	counter3.elements()
	# convert to list
	list(counter3)

	del counter[key]
	counter[missing-key] = 0

collections.deque
	not thread safe
	fast appends
	appends/pops on either side
	can use max size to get n elements from the end
	

Queue.Queue
	thread safe	


collections.defaultdict:




float("inf")


# don't have to explicitly return parameter args
paths = []
	call_a_function(params, paths):
		do something ...
		return # don't have to return paths
fnc(params, paths)


# Don't have to use slice when passing copies of params
fnc(some_list + [curr_variable])


FILTER:
	validInputs = filter(function, inputs) #returns all input items that returns True






	