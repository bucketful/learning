from collections import Counter
from collections import defaultdict

def intersection_Counter(a, b):
    counterA = Counter(a)
    counterB = Counter(b)
    counterIntr = counterA & counterB
    return list(counterIntr.elements())
    # a = [4,6,3,6,9]
    # b = [3,5,6,3,6,6]
    # print intersection(a, b)

def intersection_defaultdict(a, b):
    result = []
    dictA = defaultdict(int)
    dictB = defaultdict(int)
    for item in a:
        dictA[item] += 1
    for item in b:
        dictB[item] += 1
    for item in dictA:
        if item in dictB:
            result += [item] * min(dictA[item], dictB[item])
    return result

def intersection_sorting(a, b):
    result = []
    i = j = 0
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            i += 1
        elif a[i] > b[j]:
            j += 1
        else:
            # add if i == 0 or a[i-1] != a[i]: to remove dups
            result.append(a[i])
            i += 1
            j += 1
    return result
    # a = [3,4,6,6,9]
    # b = [3,3,5,6,6,6,9]
    # print intersection_sorted_input(a, b)

def threeSum(self, nums): # ***
    result = []
    if len(nums) < 3:
        return result
    nums.sort()
    
    for i, x in enumerate(nums):
        if i > 0 and x == nums[i-1]:
            continue
            
        j, k = i+1, len(nums)-1
        while j < k:
            twosum = nums[j] + nums[k]
            if twosum < -x:
                j += 1
            elif twosum > -x:
                k -= 1
            else:
                result.append([x, nums[j], nums[k]])
                while j < k and nums[j] == nums[j+1]:
                    j += 1
                while j < k and nums[k] == nums[k-1]:
                    k -= 1
                j += 1
                k -= 1
    return result
       
def validPalindrome(s):
    s = s.lower()
    l, r = 0, len(s) - 1
    
    while l < r:
        if s[l].isalnum() and s[r].isalnum():
            if s[l] != s[r]:
                return False
            l += 1
            r -= 1
        if not s[l].isalnum():
            l += 1
        if not s[r].isalnum():
            r -= 1
    return True 

def isNumber(s):
	'''
	Valid:' -53.5e93  ', '0', '.3'
	Invalid: 'e9', '1e', ' 1 a', ' -+3'
	'''
    dotSeen = numSeen = expSeen = False
    s = s.strip()
    
    for i, c in enumerate(s):
        # +/-
        if c in ['+', '-']:
            if not (i == 0 or s[i - 1] == 'e'):
                return False
        # .
        elif c == '.':
            if dotSeen or expSeen:
                return False
            dotSeen = True
        # e
        elif c == 'e':
            if expSeen or not numSeen:
                return False
            expSeen = True
            numSeen = False
        # digit
        elif c.isdigit():
            numSeen = True
        # else
        else:
            return False
    return numSeen

def maxSubArrayLen(self, nums, k):
    subsum = {0:-1}
    subtotal, l, r = 0, 0, 0
    for i, x in enumerate(nums):
        subtotal += x
        if subtotal not in subsum:
            subsum[subtotal] = i
        if subtotal - k in subsum:
            if r - l < i - subsum[subtotal - k]:
                l, r = subsum[subtotal - k], i
    return r - l




# get size of input-array
# if size of input-array is less than 2
#     return 0
# set distance to 0

# for each of 32 bit positions
#     [input-array, count of ones] = getOnesAndShiftRight(input-array)
#     add to distance, count of ones * count of zeros (=size of input-array - count of ones)
# return distance
def totalHammingDistance(self, nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    m = len(nums)
    if m < 2:
        return 0
    distance = 0
    
    for i in range(32):
        ones = 0
        for i, num in enumerate(nums):
            ones += (num & 1)
            nums[i] = nums[i] >> 1
        distance += ones * (m - ones)
    return distance

# [[1, 1], 2, [1, 2]]
# []
# [1]
# [[1]]
# function(input, depth)
#     if input is none
#         return 0
#     if depth was not passed:
#             set depth to 1
#     if input is an integer:
#         return integer multiplied by depth

#     iterate through every elements in the input item
#         recursive call with params(element, depth incremented by 1)
#         add the result to the totalSum
#     return totalSum

def depthSum(nestedList, depth = 1):
    if not nestedList:
        return 0

    # if nestedList.isInteger():
    #     return nestedList * depth

    # return sum(depthSum(item, depth + 1) for x in nestedList)

    # totalSum = 0
    # for item in nestedList:
    #     totalSum += depthSum(item, depth + 1)
    # return totalSum
# print depthSum([[1, 1], 2, [1, 2]])

# init counter to 0
# init result to 0
# compute xor of two inputs
# while result is greater than 0
#     increment counter
#     update result as bitwise AND function on result and (result - 1)
# return counter
def hammingDistance(a, b):
    if a < 0 or b < 0:
        return 0
    counter = 0;
    result = a^b;

    while result:
        counter += 1;
        result &= (result - 1);
    return counter;
# print hammingDistance(1, -1);

